from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import pandas as pd 

from member_model import member

app = FastAPI()

origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/getMember")
async def getMember():

   df = pd.read_csv('member.csv')
   df = df[['name','age']]
   data = df.to_json(orient='records')

   result = {'member':data}
   return result


@app.post("/addMember")
async def addMember(item:member):
   try: 
    print(item)
    name = item.name
    age = item.age

    df = pd.read_csv('member.csv')
    item = pd.DataFrame({'name':name,'age':age},index=[0])
    df = pd.concat([df,item],ignore_index=True)
    df = df[['name','age']]
    df.index =  range(1,len(df['name'])+1) 
    df.to_csv('member.csv')
    return True
   except:
    return False